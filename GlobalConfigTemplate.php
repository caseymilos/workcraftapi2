<?php

// Debug, maintenance and cache
define('DEBUG_MODE_PORTAL', false);
define('DEBUG_MODE_ADMIN', false);
define('MAINTENANCE_MODE_PORTAL', false);
define('MAINTENANCE_MODE_ADMIN', false);
define('USE_CACHE_PORTAL', false);
define('USE_CACHE_ADMIN', false);

// Clients
define('PORTAL_URL', 'http://www.workcraft-api.ubr/');
define('ADMIN_URL', 'http://admin.workcraft-api.ubr/');
define("CDN_URL", "http://cdn.workcraft-api.ubr/");
define("CDN_PATH", dirname(__FILE__) . "/Web/CDN/");

// Languages and gettext
define('PROJECT_DIR', realpath('./'));
define('LOCALE_DIR', PROJECT_DIR . '/Locale');
define('DEFAULT_LOCALE', 'en_US');
define('DEFAULT_LANGUAGE', 'en');

// SMTP
define('USE_SMTP', true);
define('SMTP_HOST', 'mailcluster.loopia.se');
define('SMTP_PORT', 587);
define('SMTP_USERNAME', 'framework@ubrium.com');
define('SMTP_PASSWORD', 'fr4m5w0rk');