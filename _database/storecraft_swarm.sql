/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : storecraft_swarm

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-04-13 22:35:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `access_token_types`
-- ----------------------------
DROP TABLE IF EXISTS `access_token_types`;
CREATE TABLE `access_token_types` (
  `AccessTokenTypeId` int(10) NOT NULL,
  `Caption` varchar(50) NOT NULL,
  PRIMARY KEY (`AccessTokenTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of access_token_types
-- ----------------------------
INSERT INTO `access_token_types` VALUES ('1', 'Admin');
INSERT INTO `access_token_types` VALUES ('2', 'Portal');

-- ----------------------------
-- Table structure for `articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `ArticleId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Subtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  `Picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DateCreated` datetime NOT NULL,
  `Tags` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ArticleId`),
  KEY `FK_blog_users` (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES ('1', '26', 'qwe', 'qwe', 'qwe', 'qwe', '2018-03-31 20:53:11', 'qwe');
INSERT INTO `articles` VALUES ('2', '26', 'drugi post', 'fudbal', 'ovo je drugi post', 'asd', '2018-03-31 22:24:56', 'fudbal');

-- ----------------------------
-- Table structure for `certificates`
-- ----------------------------
DROP TABLE IF EXISTS `certificates`;
CREATE TABLE `certificates` (
  `CertificateId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Date` date NOT NULL,
  PRIMARY KEY (`CertificateId`),
  KEY `FK_certificates_users` (`UserId`),
  CONSTRAINT `FK_certificates_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of certificates
-- ----------------------------

-- ----------------------------
-- Table structure for `certificate_details`
-- ----------------------------
DROP TABLE IF EXISTS `certificate_details`;
CREATE TABLE `certificate_details` (
  `CertificateDetailId` int(11) NOT NULL AUTO_INCREMENT,
  `CertificateId` int(255) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci NOT NULL,
  `Organisation` text COLLATE utf8_unicode_ci NOT NULL,
  `LangCode` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`CertificateDetailId`),
  KEY `cer_det_certificates` (`CertificateId`),
  CONSTRAINT `cer_det_certificates` FOREIGN KEY (`CertificateId`) REFERENCES `certificates` (`CertificateId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of certificate_details
-- ----------------------------

-- ----------------------------
-- Table structure for `confirmation_links`
-- ----------------------------
DROP TABLE IF EXISTS `confirmation_links`;
CREATE TABLE `confirmation_links` (
  `ConfirmationLinkId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `ConfirmationLink` varchar(250) NOT NULL,
  PRIMARY KEY (`ConfirmationLinkId`),
  KEY `confirmation_links_ibfk_1` (`UserId`) USING BTREE,
  CONSTRAINT `confirmation_links_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of confirmation_links
-- ----------------------------

-- ----------------------------
-- Table structure for `educations`
-- ----------------------------
DROP TABLE IF EXISTS `educations`;
CREATE TABLE `educations` (
  `EducationId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `DateFrom` date NOT NULL,
  `DateTo` date NOT NULL,
  `AveregeAchieved` float NOT NULL,
  `AveregeTotal` int(11) NOT NULL,
  PRIMARY KEY (`EducationId`),
  KEY `FK_educations_users` (`UserId`),
  CONSTRAINT `FK_educations_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of educations
-- ----------------------------

-- ----------------------------
-- Table structure for `education_details`
-- ----------------------------
DROP TABLE IF EXISTS `education_details`;
CREATE TABLE `education_details` (
  `EducationDetailId` int(11) NOT NULL AUTO_INCREMENT,
  `EducationId` int(11) NOT NULL,
  `Subject` text COLLATE utf8_unicode_ci NOT NULL,
  `University` text COLLATE utf8_unicode_ci NOT NULL,
  `LangCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`EducationDetailId`),
  KEY `FK_edu_details_education` (`EducationId`),
  CONSTRAINT `FK_edu_details_education` FOREIGN KEY (`EducationId`) REFERENCES `educations` (`EducationId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of education_details
-- ----------------------------

-- ----------------------------
-- Table structure for `experiences`
-- ----------------------------
DROP TABLE IF EXISTS `experiences`;
CREATE TABLE `experiences` (
  `ExperienceId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `DateFrom` date NOT NULL,
  `DateTo` date DEFAULT NULL,
  PRIMARY KEY (`ExperienceId`),
  KEY `fk_experiences_users` (`UserId`),
  CONSTRAINT `fk_experiences_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of experiences
-- ----------------------------

-- ----------------------------
-- Table structure for `experience_details`
-- ----------------------------
DROP TABLE IF EXISTS `experience_details`;
CREATE TABLE `experience_details` (
  `ExperienceDetailId` int(11) NOT NULL,
  `ExperienceId` int(11) NOT NULL,
  `Position` text,
  `Company` text,
  `Description` text,
  `LangCode` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ExperienceDetailId`),
  KEY `fk_exp_exp_det` (`ExperienceId`),
  CONSTRAINT `fk_exp_exp_det` FOREIGN KEY (`ExperienceId`) REFERENCES `experiences` (`ExperienceId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of experience_details
-- ----------------------------

-- ----------------------------
-- Table structure for `interests`
-- ----------------------------
DROP TABLE IF EXISTS `interests`;
CREATE TABLE `interests` (
  `InterestsId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`InterestsId`),
  KEY `FK_interests_users` (`UserId`),
  CONSTRAINT `FK_interests_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of interests
-- ----------------------------

-- ----------------------------
-- Table structure for `interest_details`
-- ----------------------------
DROP TABLE IF EXISTS `interest_details`;
CREATE TABLE `interest_details` (
  `InterestDetailId` int(11) NOT NULL AUTO_INCREMENT,
  `InterestId` int(11) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  `LangCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`InterestDetailId`),
  KEY `FK_int_det_interests` (`InterestId`),
  CONSTRAINT `FK_int_det_interests` FOREIGN KEY (`InterestId`) REFERENCES `interests` (`InterestsId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of interest_details
-- ----------------------------

-- ----------------------------
-- Table structure for `jobs`
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `JobId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ExperienceLevel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Technologies` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`JobId`),
  KEY `FK_jobs_users` (`UserId`),
  CONSTRAINT `jobs_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of jobs
-- ----------------------------
INSERT INTO `jobs` VALUES ('1', '26', 'FullTime', 'Mid-Level', 'Frontend Developer', 'JavaScript', 'We are looking for frontend developer with at least 3 years of experience');
INSERT INTO `jobs` VALUES ('2', '26', 'Intership', 'Beginer', 'Backend Developer', 'php', 'We offer an intership for talented beginer');

-- ----------------------------
-- Table structure for `languages`
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `LanguageId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Level` int(11) NOT NULL,
  PRIMARY KEY (`LanguageId`),
  KEY `FK_languages_users` (`UserId`),
  CONSTRAINT `FK_languages_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of languages
-- ----------------------------

-- ----------------------------
-- Table structure for `language_details`
-- ----------------------------
DROP TABLE IF EXISTS `language_details`;
CREATE TABLE `language_details` (
  `LanguageDetailId` int(11) NOT NULL AUTO_INCREMENT,
  `LanguageId` int(11) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci NOT NULL,
  `LangCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`LanguageDetailId`),
  KEY `FK_lang_det_lang` (`LanguageId`),
  CONSTRAINT `FK_lang_det_lang` FOREIGN KEY (`LanguageId`) REFERENCES `languages` (`LanguageId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of language_details
-- ----------------------------

-- ----------------------------
-- Table structure for `pages`
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `PageId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `StatusId` int(11) NOT NULL,
  `Views` int(11) DEFAULT '0',
  `LangCode` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`PageId`),
  KEY `fk_pages_users` (`UserId`),
  CONSTRAINT `fk_pages_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pages
-- ----------------------------

-- ----------------------------
-- Table structure for `password_reset_links`
-- ----------------------------
DROP TABLE IF EXISTS `password_reset_links`;
CREATE TABLE `password_reset_links` (
  `PasswordResetLinkId` int(10) NOT NULL AUTO_INCREMENT,
  `UserId` int(10) NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `ResetLink` varchar(250) NOT NULL,
  PRIMARY KEY (`PasswordResetLinkId`),
  KEY `confirmation_links_ibfk_1` (`UserId`) USING BTREE,
  CONSTRAINT `password_reset_links_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of password_reset_links
-- ----------------------------

-- ----------------------------
-- Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `PermissionId` int(11) NOT NULL AUTO_INCREMENT,
  `Caption` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`PermissionId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'Dashboard', null);
INSERT INTO `permissions` VALUES ('2', 'ViewUsers', null);
INSERT INTO `permissions` VALUES ('3', 'AddUsers', null);
INSERT INTO `permissions` VALUES ('4', 'EditUsers', null);
INSERT INTO `permissions` VALUES ('5', 'DeleteUsers', null);
INSERT INTO `permissions` VALUES ('20', 'Profile', null);

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `RoleId` int(11) NOT NULL AUTO_INCREMENT,
  `Protected` tinyint(4) NOT NULL,
  `Active` tinyint(4) NOT NULL,
  `Caption` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RoleId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', '1', '1', 'Administrator');
INSERT INTO `roles` VALUES ('2', '1', '1', 'User');
INSERT INTO `roles` VALUES ('3', '1', '1', 'Visitor');

-- ----------------------------
-- Table structure for `role_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE `role_permissions` (
  `RolePermissionId` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` int(11) NOT NULL,
  `PermissionId` int(11) NOT NULL,
  `Protected` smallint(6) NOT NULL,
  PRIMARY KEY (`RolePermissionId`),
  KEY `FK_RolePermissions_Permissions` (`PermissionId`) USING BTREE,
  KEY `FK_RolePermissions_Roles` (`RoleId`) USING BTREE,
  CONSTRAINT `role_permissions_ibfk_1` FOREIGN KEY (`PermissionId`) REFERENCES `permissions` (`PermissionId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_permissions_ibfk_2` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role_permissions
-- ----------------------------
INSERT INTO `role_permissions` VALUES ('1', '1', '1', '0');
INSERT INTO `role_permissions` VALUES ('2', '1', '20', '0');
INSERT INTO `role_permissions` VALUES ('3', '2', '20', '0');
INSERT INTO `role_permissions` VALUES ('4', '1', '2', '0');
INSERT INTO `role_permissions` VALUES ('5', '1', '4', '0');
INSERT INTO `role_permissions` VALUES ('6', '1', '5', '0');
INSERT INTO `role_permissions` VALUES ('7', '1', '3', '0');

-- ----------------------------
-- Table structure for `skills`
-- ----------------------------
DROP TABLE IF EXISTS `skills`;
CREATE TABLE `skills` (
  `SkillId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Level` int(11) NOT NULL,
  PRIMARY KEY (`SkillId`),
  KEY `FK_skills_users` (`UserId`),
  CONSTRAINT `FK_skills_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of skills
-- ----------------------------

-- ----------------------------
-- Table structure for `skill_details`
-- ----------------------------
DROP TABLE IF EXISTS `skill_details`;
CREATE TABLE `skill_details` (
  `SkillDetailId` int(11) NOT NULL AUTO_INCREMENT,
  `SkillId` int(11) NOT NULL,
  `Name` text COLLATE utf8_unicode_ci NOT NULL,
  `LangCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`SkillDetailId`),
  KEY `FK_skill_det_skills` (`SkillId`),
  CONSTRAINT `FK_skill_det_skills` FOREIGN KEY (`SkillId`) REFERENCES `skills` (`SkillId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of skill_details
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `UserId` int(10) NOT NULL AUTO_INCREMENT,
  `Email` varchar(250) NOT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `RegistrationDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ConfirmRegistration` tinyint(1) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  PRIMARY KEY (`UserId`),
  UNIQUE KEY `UniqueEmail` (`Email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'john@doe.com', '$2a$10$9dRxNcvXVsK.Etoyx9yOoO2/GdYWJWzQ4eysCy3LHMbyPVRNeDRoy', '2017-02-08 16:42:33', '1', '1');

-- ----------------------------
-- Table structure for `user_access_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `user_access_tokens`;
CREATE TABLE `user_access_tokens` (
  `UserAccessTokenId` int(11) NOT NULL AUTO_INCREMENT,
  `Token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `UserId` int(11) NOT NULL,
  `StartDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `EndDate` datetime DEFAULT NULL,
  `AccessTokenTypeId` int(10) NOT NULL,
  PRIMARY KEY (`UserAccessTokenId`),
  UNIQUE KEY `Unique_Token` (`Token`) USING BTREE,
  KEY `FK_UserAccessTokens_Users` (`UserId`) USING BTREE,
  KEY `TypeId` (`AccessTokenTypeId`) USING BTREE,
  CONSTRAINT `user_access_tokens_ibfk_1` FOREIGN KEY (`AccessTokenTypeId`) REFERENCES `access_token_types` (`AccessTokenTypeId`) ON UPDATE CASCADE,
  CONSTRAINT `user_access_tokens_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for `user_basics`
-- ----------------------------
DROP TABLE IF EXISTS `user_basics`;
CREATE TABLE `user_basics` (
  `UserBasicId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Website` varchar(255) DEFAULT NULL,
  `CountryCode` varchar(2) DEFAULT NULL,
  `Picture` varchar(255) DEFAULT NULL,
  `HeaderImage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UserBasicId`),
  KEY `fk_user_basics_users` (`UserId`),
  CONSTRAINT `fk_user_basics_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_basics
-- ----------------------------

-- ----------------------------
-- Table structure for `user_descriptions`
-- ----------------------------
DROP TABLE IF EXISTS `user_descriptions`;
CREATE TABLE `user_descriptions` (
  `UserDescriptionId` int(11) NOT NULL,
  `UserId` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `About` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UserDescriptionId`),
  KEY `fk_ser_descriptions_users` (`UserId`),
  CONSTRAINT `fk_ser_descriptions_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_descriptions
-- ----------------------------

-- ----------------------------
-- Table structure for `user_roles`
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `UserRoleId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `RoleId` int(11) NOT NULL,
  PRIMARY KEY (`UserRoleId`),
  KEY `FK_UserRoles_Users` (`UserId`) USING BTREE,
  KEY `FK_UserRoles_Roles` (`RoleId`) USING BTREE,
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`RoleId`) REFERENCES `roles` (`RoleId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES ('1', '1', '1');

-- ----------------------------
-- Table structure for `volunteer_works`
-- ----------------------------
DROP TABLE IF EXISTS `volunteer_works`;
CREATE TABLE `volunteer_works` (
  `VolunteerWorkId` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `DateFrom` date NOT NULL,
  `DateTo` date NOT NULL,
  PRIMARY KEY (`VolunteerWorkId`),
  KEY `FK_volunteer_users` (`UserId`),
  CONSTRAINT `FK_volunteer_users` FOREIGN KEY (`UserId`) REFERENCES `users` (`UserId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of volunteer_works
-- ----------------------------

-- ----------------------------
-- Table structure for `volunteer_work_details`
-- ----------------------------
DROP TABLE IF EXISTS `volunteer_work_details`;
CREATE TABLE `volunteer_work_details` (
  `VolunteerWorkDetailId` int(11) NOT NULL AUTO_INCREMENT,
  `VolunteerWorkId` int(11) NOT NULL,
  `Position` text COLLATE utf8_unicode_ci NOT NULL,
  `Organisation` text COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  `LangCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`VolunteerWorkDetailId`),
  KEY `FK_vwd_volunteer_work` (`VolunteerWorkId`),
  CONSTRAINT `FK_vwd_volunteer_work` FOREIGN KEY (`VolunteerWorkId`) REFERENCES `volunteer_works` (`VolunteerWorkId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of volunteer_work_details
-- ----------------------------
