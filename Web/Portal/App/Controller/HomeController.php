<?php

use Business\ApiControllers\UsersApiController;

class HomeController extends MVCController {

	public function GetIndex() {
		Router::Redirect("home");
	}

	public function GetHome() {
		$model = new HomeViewModel();
		$model->Users = UsersApiController::GetUsers();

		$this->RenderView("Home/Home", ["model" => $model]);
	}
}