<?php

/**
 * Class StatisticDTO
 * @property integer $UserCount
 * @property integer $AdminCount
 */
class StatisticDTO {

	public $UserCount;
	public $AdminCount;

}