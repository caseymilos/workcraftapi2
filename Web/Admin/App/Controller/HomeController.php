<?php


use Business\ApiControllers\UsersApiController;
use Business\Enums\PermissionsEnum;

class HomeController extends MVCController {

	public function GetIndex($permissions = [PermissionsEnum::Dashboard]) {
		$model = new DashboardViewModel();
		$statisticsDto = new StatisticDTO();
		$statisticsDto->UserCount = UsersApiController::CountUsers();
		$statisticsDto->AdminCount = UsersApiController::CountAdmins();
		$model->Statistics = $statisticsDto;

		$this->RenderView("Home/Dashboard", ["model" => $model]);
	}
}