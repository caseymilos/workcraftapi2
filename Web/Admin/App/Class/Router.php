<?php

/**
 * Class Router
 *
 * @property string $Controller
 * @property string $Action
 * @property array $Parameters
 * @property integer $RequestType
 */
class Router {
	public $Controller;
	public $Action;
	public $Parameters = array();
	public $RequestType;
	public $Language = "en";
	public $Route;

	public function __construct() {
		$this->Route = "home";

		/** Getting request method */
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->RequestType = "Post";
		}
		else {
			$this->RequestType = "Get";
		}

		/** Getting url path */
		if (isset($_GET['path'])) {
			$path = $_GET['path'];
		}
		else {
			$path = false;
		}

		$lang = strtok($path, '/');
		if (in_array($lang, ["en", "de"])) {
			$this->Language = $lang;
			$path = preg_replace(sprintf('/%s/', $lang), '', $path, 1);
		}
		else {
			$this->Language = DEFAULT_LANGUAGE;
		}

		$path = trim($path, "/");

		$foundRoute = false;

		if ($path == '') {
			$this->Controller = "Home";
			$this->Action = "Index";
			$foundRoute = true;
		}
		else {
			/** Finding route */
			$routes = RoutesConfig::GetRoutes();

			foreach ($routes as $routeName => $route) {
				$pattern = $route->Pattern;

				/* Converting URL pattern to regex */
				preg_match_all('/\{(.*?)\}/', $pattern, $match);

				$params = array();
				foreach ($match[1] as $key => $param) {
					list($paramName, $paramType) = explode(":", $param);

					/** Preparing array for storing parameters */
					$params[$paramName] = "";

					switch ($paramType) {
						case "integer" :
							$rule = "[0-9]+";
							break;
						case "string" :
							$rule = "[a-zA-Z0-9-._]+";
							break;
						default:
							$rule = "[a-zA-Z0-9]+";
							break;
					}

					$regexPart = "(?P<" . $paramName . ">" . $rule . ")";
					$pattern = str_replace($match[0][$key], $regexPart, $pattern);
				}

				$regex = "/^" . str_replace("/", "\\/", $pattern) . "$/";

				if (preg_match_all($regex, $path, $urlParams)) {

					$foundRoute = true;

					$this->Route = $routeName;
					foreach ($params as $key => $value) {
						$params[$key] = $urlParams[$key][0];
					}

					$this->Controller = $route->Controller;
					$this->Action = $route->Action;
					$this->Parameters = $params;
					if (count($route->Params)) {
						$this->Parameters = array_merge($this->Parameters, $route->Params);
					}
					continue;
				}
			}
		}

		if ($foundRoute == false) {
			throw new PageNotFoundException();
		}
		$this->Controller = (is_readable("Controller/" . $this->Controller . "Controller.php") ? $this->Controller : "Home");
	}

	public function RenderPage() {
		$controllerName = $this->Controller . "Controller";
		$controller = new $controllerName();
	}

	/**
	 * @param $name
	 * @param array $params
	 * @param bool $render
	 * @param null $language
	 * @return string
	 * @throws Exception
	 */
	public static function Create($name, $params = array(), $render = false, $language = null) {
		$pattern = false;

		$routes = RoutesConfig::GetRoutes();
		if (isset($routes[$name])) {
			$route = $routes[$name];
			$pattern = $route->Pattern;

			if (count($params)) {

				/* Converting URL pattern to regex */
				preg_match_all('/\{(.*?)\}/', $pattern, $match);

				if (count($match[1])) {
					foreach ($match[1] as $key => $param) {
						list($paramName, $paramType) = explode(":", $param);
						$pattern = str_replace($match[0][$key], $params[$paramName], $pattern);
					}
				}
			}
		}
		else {
			return "";
		}

		if ($language == null) {
			global $router;
			$language = $router->Language;
		}

		if ($language == DEFAULT_LANGUAGE) {
			$language = '';
		}

		$url = Config::baseurl . $language . ($language != '' ? "/" : '') . $pattern;

		if (true == $render) {
			echo $url;
			return true;
		}
		return $url;
	}

	public static function Redirect($name, $params = array()) {
		$path = static::Create($name, $params, false);
		header("Location: " . $path);
	}

	public static function GetLanguage() {
		if (isset($_GET['path'])) {
			$path = $_GET['path'];
		}
		else {
			$path = false;
		}

		$lang = strtok($path, '/');
		if (in_array($lang, ["en", "de"])) {
			return $lang;
		}
		else {
			return DEFAULT_LANGUAGE;
		}
	}
}