<?php


namespace Data\Repositories;
use Business\Models\RoleModel;

/**
 * Class RolesRepositories
 * @package Data\Repositories
 * @method static RoleModel[] Get
 * @method static RoleModel GetOne
 */
class RolesRepository extends BaseRepository {

}